#ifndef HSV_TOOLS_H
#define HSV_TOOLS_H

#include "cv.h"
#include "BlobResult.h"

IplImage* threshholdImage( IplImage* img );
CvScalar gimpHsvScalar(double h, double s, double v);
double hueTargetSet(IplImage*);
 
#endif
