#include "serial_tools.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]){

    if (argc != 3) {
        printf("Usage : %s <servo> <turn>\n", argv[0]);
        printf("Servo : 'A' or 'B'\n");
        printf("Turn : 0 to 180\n\n");
    }

    if(strlen(argv[1]) > 1 | strlen(argv[2]) != 3 ){
        printf("E: Command error\n");
        return -1;
    }
    
    printf("Setting up and sending command to servo %c to turn to %i deg\n",
            char(argv[1][0]),atoi(argv[2]));

    int fd;
    struct termios options;
    int ret = serialInit(fd,options);
    
    if (ret < 0){
        printf("E: Couldnt get serial interface\n");
        return -1;
    }

    char serial_data = serialSendCommand(fd,char(argv[1][0]),atoi(argv[2]));
    printf("I: Serial response from arduino : %c\n", serial_data) ;   
    serialClose(fd);
}

