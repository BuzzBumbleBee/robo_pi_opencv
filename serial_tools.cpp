#include "serial_tools.h"



int serialInit(int& fd ,struct termios& toptions){

    fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY ); 
    if (fd <0){
        return -1;
    }

    char serialString[2];
    int res = 0;


    cfsetispeed(&toptions, BAUDRATE);
    cfsetospeed(&toptions, BAUDRATE);

    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    toptions.c_cflag &= ~CRTSCTS;
    toptions.c_cflag &= ~HUPCL;

    toptions.c_cflag |= CREAD | CLOCAL;
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY);

    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); 
    toptions.c_oflag &= ~OPOST;

    toptions.c_cc[VMIN]  = 1;
    toptions.c_cc[VTIME] = 10;

    if( tcsetattr(fd, TCSANOW, &toptions) < 0) {
        printf("E: Error setting serial attributes");
        return -1;
    }
    
    usleep(500*1000);
    tcflush(fd, TCIFLUSH);
    
    while (res < 1) {   /* loop for input */
      res = read(fd,serialString,2);   /* returns after 5 chars have 
      been input */
      serialString[res]=0;   /* so we can printf... */
    }  

    //printf("S: < %s\n", serialString);
    usleep(500*1000);
    
    return 1;

}

int serialClose(int& fd){
    return close(fd);
}

char serialSendCommand(int device ,char servoName, int servoData){

    char buf2[] = "A000";
    int res = 0;
    int writeCount = 0;
    char serialString[2];
    memset ( serialString, 0, 2 );

    if (servoData < 100)
        sprintf(buf2, "%c0%d", servoName, servoData);
    else
        sprintf(buf2, "%c%d", servoName, servoData);

    //printf("S: > %s\n",buf2);
    writeCount = write(device,buf2,sizeof(buf2));

    while (res < 1){   /* loop for input */
      res = read(device,serialString,2);   /* returns after 5 chars have 
      been input */
      serialString[res]= 0;   /* so we can printf... */
    }

    //printf("S: < %s\n", serialString);

    return serialString[0];

}

char moventSend(int& fd, bool movement){

    if(movement){
        return serialSendCommand(fd,'B',30);
    } else {
        return serialSendCommand(fd,'B',2);
    }

}

char positionSend(int& fd, int center_x, int frame_center){
    int servoOffset = 90;
    if (center_x < frame_center){
        servoOffset = (int)((55.0/frame_center) * (frame_center - 
center_x));
        //printf("current motion from center : %d is +%d\n", frame_center, servoOffset);
        servoOffset = 90-servoOffset;
        return serialSendCommand(fd,'A',servoOffset);
    }
    else if(center_x > frame_center){
        servoOffset = (int)((55.0/frame_center) * (center_x - 
frame_center));
        //printf("current motion from center : %d is -%d\n", frame_center, servoOffset);
        servoOffset = 90+servoOffset;
        return serialSendCommand(fd,'A',servoOffset);
    } else {
        return serialSendCommand(fd,'A',servoOffset);
    }

}
