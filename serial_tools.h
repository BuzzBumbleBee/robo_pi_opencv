#ifndef SERIAL_TOOLS_H
#define SERIAL_TOOLS_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>

#if defined(__arm__)
#define SERIAL_EN 1
#endif

#define BAUDRATE B9600
#define MODEMDEVICE "/dev/ttyACM0"

int serialInit(int& device ,struct termios& oldtio);
char serialSendCommand(int device ,char servoName, int servoData);
char positionSend(int& fd, int center_x, int frame_center);
char moventSend(int& fd, bool movement);
int serialClose(int& device);

#endif
