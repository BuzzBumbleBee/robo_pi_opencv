# Define variables to be used within the build 
CFLAGS := `pkg-config --cflags opencv --libs opencv`
OPTION := -lblob
ARM_OPTIMIZE := -march=armv6zk -mfpu=vfp -mfloat-abi=hard
OPTIMIZE := -Ofast
DEBUG_OPTIONS := -DDEBUG_ENABLE
CC := g++
SHELL := /bin/bash
ARCH := $(shell uname -m)

#If armv6l is detected we enable optimisation for ARM system
ifeq ($(ARCH),armv6l)
	OPTIMIZE += $(ARM_OPTIMIZE)
endif

# define to build all using the two requierd files
all: cam_test.o hsv_tools.o serial_tools.o serial_commander.o
	$(CC) -o object_track cam_test.o hsv_tools.o serial_tools.o $(CFLAGS) $(OPTION) $(OPTIMIZE)
	$(CC) -o serial_commander serial_commander.o serial_tools.o $(OPTIMIZE)

# define to build the cam_test files
cam_test.o: cam_test.cpp
	$(CC) -c $< $(CFLAGS) $(OPTION) $(OPTIMIZE)

# define to build the hsv_tools files
hsv_tools.o: hsv_tools.cpp
	$(CC) -c $< $(CFLAGS) $(OPTION) $(OPTIMIZE)

# define to build the hsv_tools files
serial_tools.o: serial_tools.cpp
	$(CC) -c $< $(OPTIMIZE)

serial_commander.o: serial_commander.cpp
	$(CC) -c $< $(OPTIMIZE)

# define to clean the directory
clean:
	rm -f object_track serial_commander *.o


