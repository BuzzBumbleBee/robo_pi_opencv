#include "cv.h"
#include "BlobResult.h"
#include "hsv_tools.h"

IplImage* threshholdImage( IplImage* img )
{
    // Create an HSV format image from image passed
    IplImage* hsvImage = cvCreateImage( cvGetSize( img ),
                                        8,
                                        3);

    cvCvtColor( img, hsvImage, CV_BGR2HSV );

    // Create binary thresholded image, using a hav range
    IplImage* threshImage = cvCreateImage(cvGetSize( img ), 8,  1);

    cvInRangeS( hsvImage, gimpHsvScalar( 5.0 ,50.0 , 10.0  ), 
                gimpHsvScalar(25.0 , 75.0, 90.0), threshImage );

    // Delete unused image
    cvReleaseImage( &hsvImage );
    cvErode(threshImage, threshImage, NULL, 2);
    cvDilate(threshImage, threshImage, NULL, 5);

    return threshImage;
}


CvScalar gimpHsvScalar(double h, double s, double v){

    h = h/2;
    s = 255 * (s/100);
    v = 255 * (v/100);
#ifdef DEBUG_ENABLE
    printf("D: cvScalar of h: %f s:%f v:%f\n",h,s,v);
#endif

    return cvScalar(h ,s ,v);
}



double hueTargetSet(IplImage* img){

    CvSize imgSize = cvGetSize(img);
    CvScalar data;
    int centerY = imgSize.height/2;
    int centerX = imgSize.width/2;

    IplImage* hsvImage = cvCreateImage( imgSize, img->depth, img->nChannels);

    cvCvtColor( img, hsvImage, CV_BGR2HSV );

    cvSetImageROI( hsvImage, cvRect(centerX-5, centerY-5, 10, 10));

    IplImage* subImg = cvCreateImage(cvGetSize(hsvImage), hsvImage->depth, hsvImage->nChannels);

    cvCopy(hsvImage, subImg, NULL);

    cvResetImageROI(hsvImage);

    data = cvAvg(subImg);
    

    cvReleaseImage( &subImg );
    cvReleaseImage( &hsvImage );

    return data.val[0];
}
