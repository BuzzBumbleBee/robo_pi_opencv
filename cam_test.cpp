#include "cv.h"
#include "highgui.h"
#include "BlobResult.h"
#include "hsv_tools.h"
#include "serial_tools.h"
#include <stdio.h> 
#include <time.h>

//Un comment the follwing line to enable a debug build
//#define DEBUG_ENABLE 1

#if !defined(__arm__)
#define DISPLAY_ENABLE 1
#endif

#define GREEN "\e[32m"
#define NO_COLOR "\e[0m"

enum TASK {
    SEARCH,
    TRACK,
    AVOID,
};

FILE *logfile;

bool obstacle;

void start_search(int& fd){

    char position_respons = positionSend(fd,1,50);
    char movement_respons = moventSend(fd, true);


    if (position_respons != 'A' || movement_respons != 'B'){
        obstacle = true;
    }

    fprintf(logfile,"start_search: Serial position responce %c\n",position_respons);
    fprintf(logfile,"start_search: Serial move responce %c\n",movement_respons);
}


void start_track(int& fd, int position ,int frame_center){


    char position_respons = positionSend(fd,position,frame_center);
    char movement_respons = moventSend(fd, true);


    if (position_respons != 'A' || movement_respons != 'B'){
        obstacle = true;
    }


    fprintf(logfile,"start_track: Serial position responce %c\n",position_respons);
    fprintf(logfile,"start_track: Serial move responce %c\n",movement_respons);
}

void start_avoid(int& fd){
    fprintf(logfile,"start_track: Serial move responce %c\n",moventSend(fd, true));
    fprintf(logfile,"start_search: Serial position responce %c\n",positionSend(fd,1,50));
    sleep(2);
    fprintf(logfile,"start_search: Serial position responce %c\n",positionSend(fd,50,50));
    sleep(2);
    fprintf(logfile,"start_search: Serial position responce %c\n",positionSend(fd,99,50));
    sleep(2);
    fprintf(logfile,"start_search: Serial position responce %c\n",positionSend(fd,50,50));
    fprintf(logfile,"start_track: Serial move responce %c\n",moventSend(fd, false));
    sleep(1);

    obstacle = false;
}

int main(int argc, char *argv[]){



    if ( argc != 3 ){
        printf("\n");
        printf("Usage : %s <quality> <detect_type>\n\n",argv[0]);
        printf("Quality :\n");
        printf("\t -ss : Super Small video 160x120\n");
        printf("\t -s : Small video 320x240\n");
        printf("\t -n : Normal video 640x480\n");
        printf("\t -hd : HD video 1280x720\n");
        printf("Detect Type :\n");
        printf("\t -sub : Allow detection on sub images\n");
        printf("\t -full : Force detection on full image\n");
        printf("\n");
        return 0;
    }

    int vidH = 0;
    int vidW = 0;

    obstacle = false;

    if (!strcmp (argv[1], "-ss")){
        vidW = 160;
        vidH = 120;
    } else if (!strcmp (argv[1], "-s")){
        vidW = 320;
        vidH = 240;        vidH = 240;
    } else if (!strcmp (argv[1], "-n")) {
        vidW = 640;
        vidH = 480;
    } else if (!strcmp (argv[1], "-hd")) {
        vidW = 1280;
        vidH = 720;
    } else {
        printf("Command error : %s\n", argv[1]);
        return 0;
    }

    bool allowSub;

    if (!strcmp (argv[2], "-sub")){
        allowSub = true;
    } else if (!strcmp (argv[2], "-full")) {
        allowSub = false;
    } else {
        printf("Command error : %s\n", argv[2]);
        return 0;
    }

    CvCapture* capture = cvCaptureFromCAM( -1 );

#ifdef DEBUG_ENABLE

        printf( "*** Warning Debug Build ***\n" );

#endif

    if(!capture){
        printf( "Error getting camera interface\n" );
        return -1;
    }
 
    IplImage* frame = 0; 
    IplImage* threshFrame = 0;
    IplImage* subImg = 0;
    IplImage* threshSubImg = 0;
    int key = -1; 
    int last_x = 0 , last_y = 0, last_w = 0, last_h = 0;
    double largestBlobArea;
    double currentBlobArea;
    CBlobResult blobs;
    CBlob *currentBlob; 
    CBlob *largestBlob; 
    CvPoint point1, point2, center;
    CvRect rect; 
    bool subBlob = false;
    double currentHue = 0.00;
    time_t startTime, currentTime;
    char clearCon[] = {0x1b, 0x5b, 0x48, 0x1b, 0x5b, 0x4a, '\0'};

    TASK current_task = SEARCH;

    cvSetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH, vidW );
    cvSetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT, vidH );
    cvSetCaptureProperty(capture, CV_CAP_PROP_FPS, 25 );

    //We should output any camera properties
    int fps = ( int )cvGetCaptureProperty( capture, CV_CAP_PROP_FPS ); 
    int frame_x = ( int )cvGetCaptureProperty( capture, CV_CAP_PROP_FRAME_WIDTH ); 
    int frame_y = ( int )cvGetCaptureProperty( capture, CV_CAP_PROP_FRAME_HEIGHT ); 
    int frame_center = frame_x/2;
    logfile = fopen("log.txt", "w+");

    fprintf(logfile, "--------------------------------\n");
    fprintf(logfile, "------- RoBo-Pi Log File -------\n");
    fprintf(logfile, "--------------------------------\n");

 
    if( !( frame = cvQueryFrame( capture ) ) )
        return -1;

    //for(int i =0; i < 10; i++){

    //    frame = cvQueryFrame(capture);

    //    currentHue += hueTargetSet(frame);
    //}

    //currentHue = currentHue / 10;

    //printf("Looking for hue of %f\n",currentHue);

    int frameCount = 0;
#ifdef SERIAL_EN
    int fd;
    struct termios oldtio;
    if (serialInit(fd,oldtio)<0){
        printf("E: Error getting serial device\n");
        return -1;
    }
#endif


#ifdef DISPLAY_ENABLE
    cvNamedWindow( "video" );
    cvNamedWindow( "thresh" );

    if (allowSub)
        cvNamedWindow( "sub_area" );
#endif

    startTime = time(NULL);
    fprintf(logfile,"\nI: Capture started at %s\n", ctime(&startTime));

    while( key < 0 ) {

        subBlob = false;

        if( !( frame = cvQueryFrame( capture ) ) )
            break;

        //Check to see if there was a useable last POI and if this run allows subImage matching
        if (((last_h > 0 && last_w > 0) && ((last_x - last_w) > 0 && (last_y - last_h) > 0)) && allowSub){

            //Create the bounds (latger by 1 width and hight on either side
            cvSetImageROI( frame, cvRect(last_x-last_w, last_y-last_h, last_w*3, last_h*3 ));
            //create the sub image
            subImg = cvCreateImage(cvGetSize(frame), frame->depth, frame->nChannels);
            //Copy the frame data into the sub image
            cvCopy(frame, subImg, NULL);
            //reset the frame ROI and shoe the sub image
            cvResetImageROI(frame);

#ifdef DISPLAY_ENABLE
            cvShowImage( "sub_area", subImg );
#endif

            //threshold the sub image and check check for any blobs
            threshSubImg = threshholdImage(subImg);
#ifdef DISPLAY_ENABLE
            cvShowImage( "thresh", threshSubImg );
#endif
            blobs = CBlobResult( threshSubImg, NULL, 0 );

            //filter out any small blobs
            blobs.Filter(blobs,B_EXCLUDE,CBlobGetArea(),B_LESS,20); 

            //Cleanup the unused images
            cvReleaseImage( &threshSubImg );
            cvReleaseImage( &subImg );

            //if we find some blobs return true and skip the full image processing
            if (blobs.GetNumBlobs() > 0)
                subBlob = true;
            else {
                //if cannot use the last POI reset the the points data
                last_x = 0;
                last_y = 0;
                last_w = 0;
                last_h = 0;
            }

        } else {
            //if cannot use the last POI reset the the points data
            last_x = 0;
            last_y = 0;
            last_w = 0;
            last_h = 0;

        }

        //If there is no useable last point or no blobs are found in the area of the old image
        //we proccess the full frame
        if (!subBlob) {
            //threshold the frame, record blobs and filter
            threshFrame = threshholdImage(frame);
#ifdef DISPLAY_ENABLE
	    cvShowImage( "thresh", threshFrame );
#endif
            blobs = CBlobResult( threshFrame, NULL, 0 );
            blobs.Filter(blobs,B_EXCLUDE,CBlobGetArea(),B_LESS,20);
            cvReleaseImage( &threshFrame );
        }

        int num_blobs = blobs.GetNumBlobs();

        largestBlobArea = 0; 

        fprintf(logfile ,"D: %d blobs found\n", num_blobs);


        if (num_blobs > 0){

            for ( int i = 0; i < num_blobs; i++ ) {
                currentBlob = blobs.GetBlob(i);
                currentBlobArea = currentBlob->Area();

                if(currentBlobArea > largestBlobArea){
                    largestBlobArea = currentBlobArea;
                    largestBlob = blobs.GetBlob(i); 
                }

            }

            if(largestBlob){

                rect = largestBlob->GetBoundingBox();

                if (!subBlob){

                    point1.x = rect.x;
                    point1.y = rect.y;
                    point2.x = rect.x + rect.width;
                    point2.y = rect.y + rect.height;

                    last_x = rect.x;
                    last_y = rect.y;
                    last_w = rect.width;
                    last_h = rect.height;

                    center.x = point1.x + (rect.width/2);
                    center.y = point1.y + (rect.height/2);

                } else {
                    //If this is detected from a sub image we have to correct the data to
                    //Correspond to the full frame 
                    point1.x = rect.x+(last_x-last_w);
                    point1.y = rect.y+(last_y-last_h);
                    point2.x = rect.x + rect.width + (last_x-last_w) ;
                    point2.y = rect.y + rect.height + (last_y-last_h);

                    last_x = rect.x + (last_x-last_w);
                    last_y = rect.y + (last_y-last_h) ;
                    last_w = rect.width;
                    last_h = rect.height;

                    center.x = point1.x + (rect.width/2);
                    center.y = point1.y + (rect.height/2);
                }

#ifdef SERIAL_EN
                //fprintf(logfile,"I: Serial position responce %c\n",positionSend(fd,center.x,frame_center));
                //fprintf(logfile,"I: Serial move responce %c\n",moventSend(fd, true));
                current_task = TRACK;
#endif



                // Attach bounding rect to blob in orginal video input
                cvRectangle(frame,
                            point1,
                            point2,
                            cvScalar(100, 100, 0, 0),
                            3,
                            8,
                            0 );



                cvCircle(
                         frame, 
                         center, 
                         2, 
                         cvScalar(100, 100, 0, 0),
                         2, 
                         8, 
                         0
                         );

            }

        }else{
#ifdef SERIAL_EN   
            //fprintf(logfile,"I: Serial position responce %c\n",positionSend(fd,frame_center,frame_center));
            //fprintf(logfile,"I: Serial move responce %c\n",moventSend(fd, false));
            current_task = SEARCH;
#endif
        }

        key = cvWaitKey(10);
        frameCount++;
        currentTime = time(NULL);
        int timeTaken = currentTime - startTime;

        if(timeTaken > 0){
            fps = frameCount/timeTaken;
            printf("%sI: Current FPS = %d%s\n",GREEN,fps,NO_COLOR);
        } else {
            printf("%sI: Current FPS = N/A%s\n",GREEN,NO_COLOR);
        }

        if (timeTaken > 1){
            currentTime = time(NULL);
        }
        

        fprintf(logfile,"I: Current FPS = %d\n",fps);
        fprintf(logfile,"I: Width: %d\tHight: %d\n",frame_x,frame_y);

        if(frameCount == 10){
            cvSaveImage("output.jpg",frame);

        }

#ifdef DISPLAY_ENABLE
        cvShowImage( "video", frame );
#endif

#ifdef SERIAL_EN

        if(obstacle)
            current_task = AVOID;

        printf("%s%s",NO_COLOR,clearCon);
        printf( "%sI: Width: %d\tHight: %d%s\n",GREEN,frame_x,frame_y,NO_COLOR );

        switch(current_task){
            case SEARCH:
                start_search(fd);
                printf("%sI: No object found\n%s",GREEN,NO_COLOR);
                break;
            case TRACK:
                start_track(fd,center.x,frame_center);
                printf( "%sI: Tracking Object At X: %d Y: %d\n%s",GREEN,center.x,center.y,NO_COLOR);
                fprintf(logfile, "I: Tracking Object At X: %d Y: %d\n",center.x,center.y);
                break;
            case AVOID:
                printf( "%sI: Avoiding Object\n%s",GREEN,NO_COLOR);
                fprintf(logfile, "%sI: Avoiding Object\n%s",GREEN,NO_COLOR);
                start_avoid(fd);
                break;
        }
#endif
    }
#ifdef SERIAL_EN
    serialClose(fd);
#endif
    fclose(logfile);
    cvReleaseCapture( &capture );

}
